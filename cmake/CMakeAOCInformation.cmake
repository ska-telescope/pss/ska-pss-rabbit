# This file is reqiured for including AOC as a language in
# this project, as it's not natively supported by CMake

if(NOT SKA_RABBIT_AOC_INFORMATION_GUARD_VAR)
    set(SKA_RABBIT_AOC_INFORMATION_GUARD_VAR TRUE)
else()
    return()
endif()

set(CMAKE_AOC_OUTPUT_EXTENSION .aocx)

include(CMakeCommonLanguageInclude)

if(NOT CMAKE_AOC_COMPILE_OBJECT)
    set(CMAKE_AOC_COMPILE_OBJECT "<CMAKE_AOC_COMPILER> -o <OBJECT> <SOURCE> -v --report")
endif()

if(NOT CMAKE_AOC_LINK_EXECUTABLE)
    set(CMAKE_AOC_LINK_EXECUTABLE)
endif()

if(NOT DEFINED CMAKE_AOC_ARCHIVE_CREATE)
    set(CMAKE_AOC_ARCHIVE_CREATE "<CMAKE_MKDIR> <TARGET> && <CMAKE_CP> <OBJECTS>")
endif()

if(NOT DEFINED CMAKE_AOC_ARCHIVE_APPEND)
    set(CMAKE_AOC_ARCHIVE_APPEND "<CMAKE_CP> <OBJECTS> <TARGET>")
endif()

mark_as_advanced(CMAKE_AOC_FLAGS)
