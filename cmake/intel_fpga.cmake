#================================================================================
# Find INTEL FPGA on PANDA
# This CMAKE part verifies if the INTEL-FPGA code was enabled in PANDA.
# If this feature has been enabled in PANDA, then it will activate the
# FDAS-FPGA module to be included in RABBIT.
#================================================================================

# This variable would be available only if it was enabled on Panda.
option(ENABLE_INTEL_FPGA "Enable FDAS FPGA module in Rabbit." OFF)

if(NOT SKA_RABBIT_INTEL_FPGA_GUARD_VAR)
    set(SKA_RABBIT_INTEL_FPGA_GUARD_VAR TRUE)
else()
    return()
endif()

if(ENABLE_INTEL_FPGA)
    message(STATUS "Enabled FDAS FPGA module.")
    set(CMAKE_CXX_FLAGS "-DENABLE_INTEL_FPGA ${CMAKE_CXX_FLAGS}")
endif(ENABLE_INTEL_FPGA)
