# This file is reqiured for including AOC as a language in
# this project, as it's not natively supported by CMake

if(NOT SKA_RABBIT_TEST_AOC_COMPILER_GUARD_VAR)
    set(SKA_RABBIT_TEST_AOC_COMPILER_GUARD_VAR TRUE)
else()
    return()
endif()

if(NOT CMAKE_AOC_COMPILER)
    message(FATAL_ERROR "unable to determine aoc compiler")
endif()
set(CMAKE_AOC_COMPILER_WORKS 1 CACHE INTERNAL "")
