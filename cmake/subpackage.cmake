#===============================================================================
# subpackage
# ----------
#
# Functions for managing subpackages
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# The following variables will be defined for each architecture listed:
#
#     LIB_SRC_${ARCH}         : list of all MODULE_${NAME}_LIB_SRC_${ARCH} variables
#     TEST_UTILS_SRC_${ARCH}  : list of all MODULE_${NAME}_TEST_UTILS_SRC_${ARCH} variables
#
# Required Variables
# ^^^^^^^^^^^^^^^^^^
#
#     MODULE_${NAME}_LIB_SRC_${ARCH}          : the files to be included in the main library
#     MODULE_${NAME}_TEST_UTILS_SRC_${ARCH}   : the files to be included in the test_utils library
#
#===============================================================================

if(NOT SKA_RABBIT_SUBPACKAGE_GUARD_VAR)
    set(SKA_RABBIT_SUBPACKAGE_GUARD_VAR TRUE)
else()
    return()
endif()

# Add any new device types here
set(arch_types cpu vhdl opencl)

#===============================================================================
# subpackage
#
# brief: Add a subpackage
#
# usage: subpackage(module_name)
#
# detail: The package is expected to exist in the module_name subdirectory
#===============================================================================
macro(subpackage _subpackage_name)

    string(TOUPPER ${_subpackage_name} _SUBPACKAGE_NAME)

    if(NOT SUBPACKAGE_PATH)
        set(_subpackage_parent "")
        set(_subpackage_namespace_parent "")
        set(SUBPACKAGE_PATH "${_subpackage_name}")
        set(SUBPACKAGE_NAMESPACE "${_subpackage_name}")
        set(_subpackage_guard_parent "")
        set(subpackage_guard "${_subpackage_name}")
    else()
        set(_subpackage_parent ${SUBPACKAGE_PATH})
        set(SUBPACKAGE_PATH "${SUBPACKAGE_PATH}/${_subpackage_name}")
        set(_subpackage_namespace_parent "${SUBPACKAGE_NAMESPACE}")
        set(SUBPACKAGE_NAMESPACE "${SUBPACKAGE_NAMESPACE}::${_subpackage_name}")
        set(_subpackage_guard_parent "${subpackage_guard}")
        set(subpackage_guard "${subpackage_guard}_${_subpackage_name}")
    endif()

    # Don't propagate working variables into subdirectories
    foreach(_arch ${arch_types})

        string(TOUPPER ${_arch} _ARCH)

        set(_tmp_lib_src_${_arch} ${LIB_SRC_${_ARCH}})
        set(LIB_SRC_${_ARCH} "")
        set(_tmp_test_utils_src_${_arch} ${TEST_UTILS_SRC_${_ARCH}})
        set(TEST_UTILS_SRC_${_ARCH} "")

    endforeach()

    add_subdirectory(${_subpackage_name})

    foreach(_arch ${arch_types})

        string(TOUPPER ${_arch} _ARCH)

        set(LIB_SRC_${_ARCH} ${_tmp_lib_src_${_arch}})
        set(TEST_UTILS_SRC_${_ARCH} ${_tmp_test_utils_src_${_arch}})

        set(_mod_list_${_arch} "")
        foreach(_src ${MODULE_${_SUBPACKAGE_NAME}_LIB_SRC_${_ARCH}})
            list(APPEND _mod_list_${_arch} "${_subpackage_name}/${_src}")
        endforeach()
        list(APPEND LIB_SRC_${_ARCH} ${_mod_list_${_arch}})

        set(_mod_utils_list_${_arch} "")
        foreach(_src ${MODULE_${_SUBPACKAGE_NAME}_TEST_UTILS_SRC_${_ARCH}})
            list(APPEND _mod_utils_list_${_arch} "${_subpackage_name}/test_utils/${_src}")
        endforeach()
        list(APPEND TEST_UTILS_SRC_${_ARCH} ${_mod_utils_list_${_arch}})

    endforeach()

    # Add header install target
    file(GLOB _include_files "${CMAKE_CURRENT_SOURCE_DIR}/${_subpackage_name}/*.h")
    install(FILES ${_include_files} DESTINATION ${INCLUDE_INSTALL_DIR}/${SUBPACKAGE_PATH})

    # Restore subpackage tracking variables to the parent scope
    set(SUBPACKAGE_PATH "${_subpackage_parent}")
    set(SUBPACKAGE_NAMESPACE "${_subpackage_namespace_parent}")
    set(subpackage_guard "${_subpackage_guard_parent}")

endmacro(subpackage)

#===============================================================================
# add_test_utils_dir
#
# brief: Adds the test_utils subdirectory to the build and sets global
#        variables for each architecture (i.e. CPU, FPGA, etc) listed
#
# usage: add_test_utils_dir()
#===============================================================================
function(add_test_utils_dir)
    get_filename_component(name ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    string(TOUPPER ${name} _NAME)
    add_subdirectory(test_utils)
    foreach(arch ${arch_types})
        string(TOUPPER ${arch} ARCH)
        set(MODULE_${_NAME}_TEST_UTILS_SRC_${ARCH}
            ${MODULE_${_NAME}_TEST_UTILS_SRC_${ARCH}}
            PARENT_SCOPE
        )
    endforeach()
endfunction(add_test_utils_dir)

#===============================================================================
# namespace_generator_start
#
# brief: Generates an opening namespace declaration block in a file
#
# usage: namespace_generator_start(subpackage_namespace_file namespace_a namespace_b)
#
# details: `namespace_generator_start(subpackage_namespace_start.h namespace_a namespace_b)`
#          will produce the subpackage_namespace_start.h file with:
#              namespace namespace_a {
#              namespace namespace_b {
#===============================================================================
function(namespace_generator_start file_name)
    file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${file_name}.gen" "// File generated by subpackage.cmake  - do not edit\n")
    foreach(namespace ${ARGN})
        string(REPLACE "/" ";" namespace_list ${namespace})
        foreach(namespace_element ${namespace_list})
            file(APPEND "${CMAKE_CURRENT_BINARY_DIR}/${file_name}.gen" "namespace ${namespace_element} {\n")
        endforeach()
    endforeach()
    configure_file("${CMAKE_CURRENT_BINARY_DIR}/${file_name}.gen" "${file_name}" COPYONLY)
endfunction(namespace_generator_start)

#===============================================================================
# namespace_generator_end
#
# brief: Generates a closing namespace declaration block in a file
#
# usage: namespace_generator_end(subpackage_namespace_file namespace_a namespace_b)
#
# details: `namespace_generator_end(subpackage_namespace_end.h namespace_a namespace_b)`
#          will produce the subpackage_namespace_end.h file with:
#              } // namespace namespace_b
#              } // namespace namespace_a
#===============================================================================
function(namespace_generator_end file_name)
    file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${file_name}.gen" "// File generated by subpackage.cmake  - do not edit\n")
    set(reverse_argn)
    foreach(namespace ${ARGN})
        string(REPLACE "/" ";" namespace_list ${namespace})
        foreach(namespace_element ${namespace_list})
            list(INSERT reverse_argn 0 ${namespace_element})
        endforeach()
    endforeach()
    foreach(namespace ${reverse_argn})
        file(APPEND "${CMAKE_CURRENT_BINARY_DIR}/${file_name}.gen" "} // namespace ${namespace}\n")
    endforeach()
    configure_file("${CMAKE_CURRENT_BINARY_DIR}/${file_name}.gen" "${file_name}" COPYONLY)
endfunction(namespace_generator_end)
