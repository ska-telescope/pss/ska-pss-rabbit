if(NOT SKA_RABBIT_BOOST_GUARD_VAR)
    set(SKA_RABBIT_BOOST_GUARD_VAR TRUE)
else()
    return()
endif()

if(PANDA_CMAKE_MODULE_PATH)
    include("${PANDA_CMAKE_MODULE_PATH}/boost.cmake")
else()
    message(FATAL_ERROR "Dependency Error: include boost.cmake after panda has been found")
endif()

# Find boost
if(BOOST_ROOT)
    # Workaround for cmake version 3.24 which ignores
    # BOOST_ROOT if a BoostConfig.cmake file is found
    set(Boost_NO_SYSTEM_PATHS TRUE)
endif()
find_package(Boost COMPONENTS filesystem system program_options REQUIRED)

# Set some variables to make the output of the CMake-included FindBoost match our conventions
set(BOOST_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
set(BOOST_LIBRARY_DIRS ${Boost_LIBRARY_DIRS})
set(BOOST_LIBRARY_DIR_RELEASE ${Boost_LIBRARY_DIR_RELEASE})
set(BOOST_LIBRARY_DIR_DEBUG ${Boost_LIBRARY_DIR_DEBUG})
set(BOOST_LIBRARIES ${Boost_LIBRARIES})
set(BOOST_DIR ${Boost_DIR})
set(BOOST_MAJOR_VERSION ${Boost_MAJOR_VERSION})
set(BOOST_MINOR_VERSION ${Boost_MINOR_VERSION})

option(BOOST_ASIO_DEBUG "Set to TRUE to enable Boost Asio handler tracking" OFF)

if(BOOST_ASIO_DEBUG)
    set(BOOST_ASIO_DEBUG true) # Lower-case TRUE here, because it needs to be a C++ type
    add_definitions(-DBOOST_ASIO_ENABLE_HANDLER_TRACKING)
else()
    set(BOOST_ASIO_DEBUG false) # Lower-case FALSE here, because it needs to be a C++ type
endif()

if(BOOST_MAJOR_VERSION EQUAL 1 AND BOOST_MINOR_VERSION LESS 56)
    # Pick up boost::align from the local thirdparty copy
    list(APPEND BOOST_INCLUDE_DIRS "${PROJECT_SOURCE_DIR}/thirdparty/boost/1.56")
endif()
