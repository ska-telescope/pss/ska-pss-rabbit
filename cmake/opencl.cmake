# OpenCL development toolkit and configuration

if(NOT SKA_RABBIT_OPENCL_GUARD_VAR)
    set(SKA_RABBIT_OPENCL_GUARD_VAR TRUE)
else()
    return()
endif()

if(ENABLE_OPENCL)

    find_path(OpenCL_INCLUDE_DIR
              NAMES
                  CL/cl.h OpenCL/cl.h
              PATHS
                  ${OpenCL_INCLUDE_DIR}
                  ${OpenCL_INSTALL_DIR}
                  ${OpenCL_INSTALL_DIR}/hld
                  ${ALTERAOCLSDKROOT}
                  ${INTELFPGAOCLSDKROOT}
                  ENV "PROGRAMFILES(X86)"
                  ENV AMDAPPSDKROOT
                  ENV NVSDKCOMPUTE_ROOT
                  ENV ATISTREAMSDKROOT
                  /usr/local
                  /usr
              PATH_SUFFIXES
                  include
                  host/include
                  OpenCL/common/inc
                  "AMD APP/include"
    )

    find_library(OpenCL_LIBRARY
        NAMES OpenCL
        PATHS
            ${OpenCL_LIBRARY_DIR}
            ${OpenCL_INSTALL_DIR}
            ${OpenCL_INSTALL_DIR}/host
            ${OpenCL_INSTALL_DIR}/hld/host
            ${ALTERAOCLSDKROOT}
            ${INTELFPGAOCLSDKROOT}
        PATH_SUFFIXES
            lib64
            lib
            "AMD APP/lib/x86"
            lib/x86
            lib/Win32
            linux64/lib
            host/linux64/lib
            OpenCL/common/lib/Win32
    )
    set(OpenCL_LIBRARIES ${OpenCL_LIBRARY})
    set(OpenCL_INCLUDE_DIRS ${OpenCL_INCLUDE_DIR})

    include(FindPackageHandleCompat)
    find_package_handle_standard_args(OpenCL DEFAULT_MSG OpenCL_LIBRARIES OpenCL_INCLUDE_DIRS)

    if(NOT OPENCL_FOUND)
        set(OpenCL_LIBRARIES)
        set(OpenCL_TEST_LIBRARIES)
    endif()

    mark_as_advanced(OpenCL_LIBRARIES OpenCL_TEST_LIBRARIES OpenCL_INCLUDE_DIR)

    include_directories(${OpenCL_INCLUDE_DIRS})

    set(CMAKE_CXX_FLAGS "-DENABLE_OPENCL ${CMAKE_CXX_FLAGS}")
    list(APPEND DEPENDENCY_LIBRARIES ${OpenCL_LIBRARIES})
    message("OpenCL Libraries: ${OpenCL_LIBRARY}")

endif()
