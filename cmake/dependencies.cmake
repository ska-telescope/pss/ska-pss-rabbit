if(NOT SKA_RABBIT_DEPENDENCIES_GUARD_VAR)
    set(SKA_RABBIT_DEPENDENCIES_GUARD_VAR TRUE)
else()
    return()
endif()

include(doxygen)

include(compiler_settings)
include(cmake/googletest.cmake)

find_package(Panda REQUIRED)
list(APPEND CMAKE_MODULE_PATH "${PANDA_CMAKE_MODULE_PATH}")
include(dependency_register)

include("${PANDA_CMAKE_MODULE_PATH}/pandaConfiguration.cmake")
load_dependency(cuda) # Required to define some fake cmake macros if not enabled

required_dependency(boost)

# Other dependencies
include(cmake/doxygen.cmake)
include(cmake/image_manager.cmake)
if(ENABLE_OPENCL)
    include(cmake/aoc/aoc_tools.cmake)
    include(cmake/opencl.cmake)
endif()

include_directories(SYSTEM ${BOOST_INCLUDE_DIRS} ${PANDA_INCLUDE_DIR})

set(DEPENDENCY_LIBRARIES
    ${PANDA_LIBRARIES}
    ${BOOST_LIBRARIES}
    ${OpenCL_LIBRARIES}
)

# Generate configuration-dependent products
generate_configuration_header("SKA_RABBIT_")
generate_export_project_configuration()
