# Adapted from https://github.com/bilke/cmake-modules/blob/master/CodeCoverage.cmake

if(NOT SKA_RABBIT_CODE_COVERAGE_GUARD_VAR)
    set(SKA_RABBIT_CODE_COVERAGE_GUARD_VAR TRUE)
else()
    return()
endif()

if(CMAKE_BUILD_TYPE MATCHES profile)

    #============================================
    # coverage_target
    #
    # brief: function to generate a coverage report of the specified target
    #
    # usage: coverage_target(target)
    #
    # details: the target will be executed (e.g. 'make targetname') and coverage report generated for that launch
    #============================================
    macro(coverage_target _targetname)

        set(_outputname coverage_${_targetname})
        set(_outputdir "coverage")
        set(_coverage_target coverage_report_${_targetname})
        add_custom_target(${_coverage_target}
                          COMMAND ${LCOV_PATH} --directory ${_outputdir} --zerocounters # Cleanup lcov
                          COMMAND ${CMAKE_MAKE_PROGRAM} ${_targetname} ${ARGN} # Run the target
                          COMMAND ${LCOV_PATH} --directory . --capture --output-file ${_outputdir}/${_outputname}.info # Capture the lcov counters and generate report
                          COMMAND ${LCOV_PATH} --remove ${_outputdir}/${_outputname}.info 'rabbit/*' 'test/*' '/usr/*' --output-file ${_outputdir}/${_outputname}.info.cleaned
                          COMMAND ${GENHTML_PATH} -o ${_outputdir}/${_outputname} ${_outputdir}/${_outputname}.info.cleaned
                          COMMAND ${CMAKE_COMMAND} -E remove ${_outputdir}/${_outputname}.info ${_outputdir}/${_outputname}.info.cleaned
                          WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )

        # Show info where to find the report
        add_custom_command(TARGET coverage_report_${_targetname} POST_BUILD
                           COMMAND ;
                           COMMENT "see ${_outputdir}/${_outputname}/index.html for the coverage report."
        )

    endmacro(coverage_target)

    # Find the coverage generators
    find_program(GCOV_PATH gcov)
    find_program(LCOV_PATH lcov)
    find_program(GENHTML_PATH genhtml)

    # Sanity checking
    if(NOT GCOV_PATH) 
        message(FATAL_ERROR "gcov not found. specify with GCOV_PATH")
    endif() 

    if(NOT LCOV_PATH) 
        message(FATAL_ERROR "lcov not found. specify with LCOV_PATH")
    endif() 

    if(NOT GENHTML_PATH)
        message(FATAL_ERROR "genhtml not found. specify with GENHTML_PATH")
    endif()

    file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/coverage)

else()

    # if coverage is not available, output a suitable message if someone tries to build the target 
    macro(coverage_target _targetname)
        set(_coverage_target coverage_report_${_targetname})
        add_custom_target(${_coverage_target}
                          COMMAND ${CMAKE_COMMAND} -E echo
                          COMMENT "target ${_coverage_target} requires a profile build. Did you forget to set CMAKE_BUILD_TYPE=profile?"
        )
    endmacro(coverage_target)

endif()

coverage_target(test)
