# Store the git hash of the current head
# Based on blog of David Gobbi (http://www.cognitive-antics.net/?p=816)

if(NOT SKA_RABBIT_GIT_VERSION_GUARD_VAR)
    set(SKA_RABBIT_GIT_VERSION_GUARD_VAR TRUE)
else()
    return()
endif()

if(EXISTS "${PROJECT_SOURCE_DIR}/.git/HEAD")
    file(READ "${PROJECT_SOURCE_DIR}/.git/HEAD" PROJECT_SOURCE_HASH) # This can be either a path to a file for a branch or a hash
    if("${PROJECT_SOURCE_HASH}" MATCHES "^ref:") # In this case, the HEAD file contains a path to a file for a branch
        # Strip the file path from the line
        string(REGEX REPLACE "^ref: *([^ \n\r]*).*" "\\1" git_branch_path "${PROJECT_SOURCE_HASH}")
        # Set the full path to the file containing the hash
        set(git_hash_file "${PROJECT_SOURCE_DIR}/.git/${git_branch_path}")
        if(EXISTS "${git_hash_file}")
            # Read the hash from the file
            file(READ "${git_hash_file}" PROJECT_SOURCE_HASH)
        endif()
    endif()
    # Strip whitespace from PROJECT_SOURCE_HASH,
    # which is now a hash of the current git head
    string(STRIP "${PROJECT_SOURCE_HASH}" PROJECT_SOURCE_HASH)
endif()
