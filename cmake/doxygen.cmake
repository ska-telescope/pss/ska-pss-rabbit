if(NOT SKA_RABBIT_DOXYGEN_GUARD_VAR)
    set(SKA_RABBIT_DOXYGEN_GUARD_VAR TRUE)
else()
    return()
endif()

if(ENABLE_DOC)
    find_package(Doxygen)
    if(DOXYGEN_FOUND)
        set(DOXYGEN_BUILD_DIR ${CMAKE_BINARY_DIR}/doc)
        file(MAKE_DIRECTORY ${DOXYGEN_BUILD_DIR})
        configure_file(${CMAKE_SOURCE_DIR}/cmake/Doxyfile.in ${CMAKE_BINARY_DIR}/Doxyfile @ONLY)
        # Add a target to generate API documentation with Doxygen
        add_custom_target(doc
                          ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile
                          WORKING_DIRECTORY ${DOXYGEN_BUILD_DIR}
                          COMMENT "Generating API documentation with Doxygen"
                          VERBATIM
        )

        install(DIRECTORY ${DOXYGEN_BUILD_DIR}/
                DESTINATION ${DOC_INSTALL_DIR}
                PATTERN "${DOXYGEN_BUILD_DIR}/*"
        )
    else()
        add_custom_target(doc
                          COMMAND ${CMAKE_COMMAND} -E echo
                          COMMENT "Doxygen not found, no doc target configured!"
                          VERBATIM
        )
    endif()
else()
    add_custom_target(doc
                      COMMAND ${CMAKE_COMMAND} -E echo
                      COMMENT "No doc target configured. Rebuild with -DENABLE_DOC=true"
                      VERBATIM
    )
endif()
