# The MIT License (MIT)
#
# Copyright (c) 2024 The SKA organisation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

if(ENABLE_OPENCL)

    option(ALTERA_FPGA_EMULATION "set to true to enable code generation for the intel emulator" OFF)

    # List of boards to compile for, available boards
    # can be checked by the `aocl diagnose` command
    if(ALTERA_FPGA_EMULATION)
        set(altera_boards
            emulator # check emulator installation, driver (.icd) or library (.so)
        )
        add_definitions(-DALTERA_FPGA_EMULATION)
    else()
        set(altera_boards
            a10pl4_dd4gb_gx115  # Bittware's A10PL4 card, Bitware Support Package 17.1.1
            #a10gx              # Intel's dev kit, Arria-10 card
            #p385a_sch_ax115    # Nallatech Arria-10, check BSP installation
            #p510t_sch_ax115    # Nallatech Dual FPGA Arria-10, check BSP installation
            emulator            # emulator can also be added here along with devices
        )
    endif()

    # Find the AOC compiler
    enable_language(AOC)

    # Set the compiled .aoc file to the default if not set by the user
    if(NOT CMAKE_AOC_OUTPUT_EXTENSION)
        set(CMAKE_AOC_OUTPUT_EXTENSION ".aocx")
    endif()

    if(NOT CMAKE_AOC_COMPILER)
        message(FATAL_ERROR "Unable to determine aoc compiler")
    endif()

    #============================================
    # aoc_meta_cpp
    #
    # brief: write metafiles for a specific image manager target
    #
    # usage: aoc_meta_cpp(image_manager_target)
    #
    # details: this will insert the given image manager target name
    #          into a new class file using the class file templates,
    #          which allows the code to dynamically build only the
    #          required image manager.
    #============================================
    macro(aoc_meta_cpp _image_manager_name)

        set(image_manager ${_image_manager_name})
        set(_namespace_start_file subpackage_namespace_start.h)
        namespace_generator_start(${_namespace_start_file}
                                  ska
                                  ${PROJECT_NAME}
                                  ${SUBPACKAGE_PATH}
                                 )

        set(_namespace_end_file subpackage_namespace_end.h)
        namespace_generator_end(${_namespace_end_file}
                                ska
                                ${PROJECT_NAME}
                                ${SUBPACKAGE_PATH}
                                )
        set(image_path "images/${SUBPACKAGE_PATH}")

        # Generate meta info files
        configure_file(${CMAKE_SOURCE_DIR}/cmake/aoc/ImageManager.h.in
                       "${CMAKE_CURRENT_BINARY_DIR}/${_image_manager_name}.h"
                      )
        configure_file(${CMAKE_SOURCE_DIR}/cmake/aoc/ImageManager.cpp.in
                       "${CMAKE_CURRENT_BINARY_DIR}/${_image_manager_name}.cpp"
                      )
        list(APPEND LIB_SRC_CPU ${_image_manager_name}.cpp)
        set(LIB_SRC_CPU ${LIB_SRC_CPU} PARENT_SCOPE)

    endmacro(aoc_meta_cpp)

    #============================================
    # aoc_compile_board
    #
    # brief: generate .aocx file for a specific board
    #
    # usage: aoc_compile_board(image_name
    #                          include_file_template
    #                          image_initialiser_file_template
    #                          aoc_src
    #                          target_board
    #                         )
    #
    # details: this will generate one .aocx file from a single .aoc source file
    #          for one specific board. This macro is wrapped in a nested loop in
    #          `aoc_compile(...)` in order to build all images for all boards, so
    #          this never needs to be called directly unless the developer has a
    #          specific need.
    #============================================
    macro(aoc_compile_board _target _algo_include_file _algo_image_initialiser_file _aoc_src board)

        set(output_file_name ${_target}${CMAKE_AOC_OUTPUT_EXTENSION})
        set(board ${board}) # Needs to be done so that the ${board} propagates
                            # into the configure_file() call

        # Generate the meta-information for this board/algo
        set(_image_path "images/${SUBPACKAGE_PATH}")
        set(_algo_header_file "${board}/AlgoTraits.h")
        set(_algo_cpp_file "${board}/AlgoTraits.cpp")
        configure_file(${CMAKE_SOURCE_DIR}/cmake/aoc/AOCImageMetaCpp.h.in
                       ${CMAKE_CURRENT_BINARY_DIR}/${_algo_header_file}
                      )
        configure_file(${CMAKE_SOURCE_DIR}/cmake/aoc/AOCImageMetaCpp.cpp.in
                       ${CMAKE_CURRENT_BINARY_DIR}/${_algo_cpp_file}
                      )
        list(APPEND LIB_SRC_CPU ${_algo_cpp_file})

        file(APPEND ${_algo_include_file}
                    "#include \"${PROJECT_NAME}/${SUBPACKAGE_PATH}/${_algo_header_file}\"\n"
            )
        file(APPEND ${_algo_image_initialiser_file}
                    "add(${SUBPACKAGE_NAMESPACE}::${board}::AlgoTraits());\n"
            )

        # Add include directores to aoc compiler flags
        get_property(_include_dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                                   PROPERTY INCLUDE_DIRECTORIES
                    )
        foreach(_include_dir ${_include_dirs})
            list(APPEND CMAKE_AOC_COMPILE_FLAGS -I"${_include_dir}")
        endforeach()

        # Add definitions
        get_property(_compiler_defs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                                    PROPERTY COMPILE_DEFINITIONS
                    )
        foreach(_compiler_def ${_compiler_defs})
            list(APPEND CMAKE_AOC_COMPILE_FLAGS -D${_compiler_def})
        endforeach()

        # Generate the FPGA image. The CMake variable ALTERA_FPGA_EMULATION
        # enables switching between emulation & FPGA card.
        if("${board}" STREQUAL "emulator")
            add_custom_command(OUTPUT "${output_file_name}"
                               DEPENDS "${_aoc_src}"
                               COMMAND ${CMAKE_AOC_COMPILER}
                                       ${CMAKE_AOC_COMPILE_FLAGS}
                                       -v -march=${board}
                                       -o "${output_file_name}"
                                       "${_aoc_src}"
                               COMMENT "Generating Intel/Altera FPGA emulator image"
            )
        else()
            add_custom_command(OUTPUT "${output_file_name}"
                               DEPENDS "${_aoc_src}"
                               COMMAND ${CMAKE_AOC_COMPILER}
                                       ${CMAKE_AOC_COMPILE_FLAGS}
                                       -v -board=${board}
                                       -board-package=${AOC_BSP}
                                       -o "${output_file_name}"
                                       "${_aoc_src}"
                               COMMENT "Generating Intel/Altera FPGA for the board ${board}"
            )
        endif()
        add_custom_target(${_target}
                          DEPENDS ${output_file_name}
                          ${CMAKE_CURRENT_BINARY_DIR}/${_algo_header_file}
                         )

        # Create image install target
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${output_file_name}
                DESTINATION ${MODULES_INSTALL_DIR}/${_image_path}
               )

    endmacro(aoc_compile_board)

    #============================================
    # aoc_compile
    #
    # brief: compile OpenCL firmware for all FPGA boards
    #
    # usage: aoc_compile(target aoc_srcs)
    #
    # details: this macro is usable within the CMakeLists.txt of a subpackage.
    #          It will cause each .aoc file to be compiled into an .aocx file
    #          compatible with each of the boards in the altera_boards list.
    #          Install targets will be generated to store these in the
    #          MODULES_INSTALL_DIR/images/ folder. An ImageManager class will
    #          be generated with knowldege of these images (with the target
    #          name). These generated classes are not exported by default, so
    #          either add your own install target or use them only in compiled
    #          code, avoiding their use in your own exported headers.
    #============================================
    macro(aoc_compile _target _aoc_srcs)

        set(_algo_includes "${CMAKE_CURRENT_BINARY_DIR}/AlgoMetaIncludes.h")
        set(algo_includes_header "AlgoMetaIncludes.h")
        set(_algo_includes_gen "${_algo_includes}.gen")
        set(_algo_image_initialiser_file
            "${CMAKE_CURRENT_BINARY_DIR}/AlgoImageInitialiser.h"
           )
        set(algo_image_initialiser_file_header "AlgoImageInitialiser.h")
        set(_algo_image_initialiser_file_gen
            "${_algo_image_initialiser_file}.gen"
           )
        file(WRITE ${_algo_includes_gen}
                   "// Generated file for target ${_target} - do not edit\n"
            )
        file(WRITE ${_algo_image_initialiser_file_gen}
                   "// Generated file for target ${_target} - do not edit\n"
             )
        add_custom_target(${_target} ALL
                          DEPENDS ${_algo_includes}
                                  ${_algo_image_initialiser_file}
                         )
        aoc_meta_cpp(${_target})

        if(NOT COMPILE_OPENCL_IMAGES)
            set(altera_boards emulator)
        endif()
        # Generate a target for each board
        foreach(_board ${altera_boards})
            foreach(_aoc_src ${_aoc_srcs})
                set(_aoc_src_file "${CMAKE_CURRENT_SOURCE_DIR}/${_aoc_src}")
                message("Generating FPGA board target ${_board} for ${_aoc_src_file}")
                aoc_compile_board("${_target}_${_board}"
                                  "${_algo_includes_gen}"
                                  "${_algo_image_initialiser_file_gen}"
                                  "${_aoc_src_file}"
                                  ${_board}
                                 )
                add_dependencies(${_target} "${_target}_${_board}")
            endforeach()
        endforeach()

        configure_file(${_algo_includes_gen} ${_algo_includes} COPYONLY)
        configure_file(${_algo_image_initialiser_file_gen}
                       ${_algo_image_initialiser_file} COPYONLY
                      )

    endmacro(aoc_compile)

endif()
