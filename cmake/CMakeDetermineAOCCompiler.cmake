# This file is reqiured for including AOC as a language in
# this project, as it's not natively supported by CMake

if(NOT SKA_RABBIT_DETERMINE_AOC_COMPILER_GUARD_VAR)
    set(SKA_RABBIT_DETERMINE_AOC_COMPILER_GUARD_VAR TRUE)
else()
    return()
endif()

set(CMAKE_AOC_COMPILER_ENV_VAR "AOC")

if(NOT CMAKE_AOC_COMPILER)
    # Prefer the environment variable
    if($ENV{${CMAKE_AOC_COMPILER_ENV_VAR}} MATCHES ".+")
        get_filename_component(CMAKE_AOC_COMPILER_LIST $ENV{${CMAKE_AOC_COMPILER_ENV_VAR}} PROGRAM PROGRAM_ARGS CMAKE_AOC_FLAGS)
        if(NOT EXISTS ${CMAKE_AOC_COMPILER_LIST})
            message(FATAL_ERROR "Could not find compiler set in environment variable ${CMAKE_AOC_COMPILER_ENV_VAR}")
        endif()
    else()
        set(CMAKE_AOC_COMPILER_LIST aoc)
    endif()

    # See if any of the compilers are reachable.
    # The user may specify a custom PATH in CMAKE_USER_AOC_COMPILER_PATH.
    if(CMAKE_USER_AOC_COMPILER_PATH)
        find_program(CMAKE_AOC_COMPILER NAMES ${CMAKE_AOC_COMPILER_LIST} PATHS ${CMAKE_USER_AOC_COMPILER_PATH} NO_DEFAULT_PATH)
    else()
        find_program(CMAKE_AOC_COMPILER NAMES ${CMAKE_AOC_COMPILER_LIST})
    endif()

    set(CMAKE_AOC_COMPILER "${CMAKE_AOC_COMPILER}" CACHE FILEPATH "AOC Compiler")
endif()

include(CMakeFindBinUtils)

# Configure the variables in this file for faster reloads
configure_file(${CMAKE_CURRENT_LIST_DIR}/CMakeAOCCompiler.cmake.in
               ${CMAKE_PLATFORM_INFO_DIR}/CMakeAOCCompiler.cmake
               @ONLY
)
