if(NOT SKA_RABBIT_IMAGE_MANAGER_GUARD_VAR)
    set(SKA_RABBIT_IMAGE_MANAGER_GUARD_VAR TRUE)
else()
    return()
endif()

#============================================
# metafile_creator
#
# brief: create a header and cpp file for an ImageManager class
#
# usage: metafile_creator(image_manager_name)
#
# details: uses existing ImageManager class template files
#          (.h and .cpp files) and inserts information
#          relveant to the given 'image_manager_name'
#============================================
function(metafile_creator image_manager_name)

    # Set the image manager name that will be inserted into the metafiles
    set(image_manager ${image_manager_name})

    # Create a generically-named file and write an opening namespace declaration block to it
    set(namespace_start_file subpackage_namespace_start.h)
    namespace_generator_start(${namespace_start_file} ska ${PROJECT_NAME} ${SUBPACKAGE_PATH})

    # Create a generically-named file and write a closing namespace declaration block to it
    set(namespace_end_file subpackage_namespace_end.h)
    namespace_generator_end(${namespace_end_file} ska ${PROJECT_NAME} ${SUBPACKAGE_PATH})

    # Set the path where the images are installed
    set(image_path "images/${SUBPACKAGE_PATH}")

    # Generate metafiles
    configure_file(${CMAKE_SOURCE_DIR}/cmake/aoc/ImageManager.h.in "${CMAKE_CURRENT_BINARY_DIR}/${image_manager_name}.h")
    configure_file(${CMAKE_SOURCE_DIR}/cmake/aoc/ImageManager.cpp.in "${CMAKE_CURRENT_BINARY_DIR}/${image_manager_name}.cpp")
    list(APPEND LIB_SRC_CPU ${image_manager_name}.cpp)
    set(LIB_SRC_CPU ${LIB_SRC_CPU} PARENT_SCOPE)

endfunction(metafile_creator)

#============================================
# image_compiler
#
# brief: function to generate an ImageManager class for
#        a given module for managing non-OpenCL images
#
# usage: image_compiler(image_manager_name)
#
# details: generate an ImageManager class for a given module named
#          'image_manager_name' with knowledge of any non-OpenCL images
#============================================
function(image_compiler image_manager_name)

    # Set the name and path of a header file that will be generated for the given 'image_manager_name'
    set(algo_includes "${CMAKE_CURRENT_BINARY_DIR}/AlgoMetaIncludes.h")
    # Set the name of the header file, which will be inserted into the generated cpp file
    set(algo_includes_header "AlgoMetaIncludes.h")
    # Set the name of the file that will be used to generate the header file
    set(algo_includes_gen "${algo_includes}.gen")
    # Set the name and path of a header file that will be generated for the given 'image_manager_name'
    set(algo_image_initialiser_file "${CMAKE_CURRENT_BINARY_DIR}/AlgoImageInitialiser.h")
    # Set the name of the header file, which will be inserted into the generated cpp file
    set(algo_image_initialiser_file_header "AlgoImageInitialiser.h")
    # Set the name of the file that will be used to generate the header file
    set(algo_image_initialiser_file_gen "${algo_image_initialiser_file}.gen")

    # Write a message to the two files we will use to generate the header files
    file(WRITE ${algo_includes_gen} "// Generated file for target ${image_manager_name} - do not edit\n")
    file(WRITE ${algo_image_initialiser_file_gen} "// Generated file for target ${image_manager_name} - do not edit\n")

    # A the two header files we will generate as dependencies of the desired ImageManager
    add_custom_target(${image_manager_name} ALL DEPENDS ${algo_includes} ${algo_image_initialiser_file})

    # Add some text to the two generator files to build up the ImageManager class for the given 'image_manager_name'
    metafile_creator(${image_manager_name})
    # As metafile_creator is a function, when it sets ${LIB_SRC_CPU}, it is set
    # here, but we need to promote it again so that it becomes gloablly set
    # outside of this function.
    set(LIB_SRC_CPU ${LIB_SRC_CPU} PARENT_SCOPE)

    # Copy the generated files to the final outputs
    configure_file(${algo_includes_gen} ${algo_includes} COPYONLY)
    configure_file(${algo_image_initialiser_file_gen} ${algo_image_initialiser_file} COPYONLY)

endfunction(image_compiler)
