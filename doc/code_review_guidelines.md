= Code Review

The review process is an analysis of the code to help improve code quality.
A good review will not only catch the usual slips/problems of style and bugs, but
should also consider the overall health of the project and how any new code fits in.
Always be on the lookout for opportunities to refactor and reuse as well as 
highlighting any flaws in our assumptions in the whole architecture.

Consider the following points when code reviewing wherever possible:

== Compilation
1) does it compile in your environment
2) does it compile on the reference environment (check skabuildmaster)

== Unit Tests
1) Is there a unit test for each class and the test compiles?
2) If there is no code to test is there adequate documentation to explain in the unitTest header file
3) Do the tests conform to the style guidelines?
4) What is the coverage?
5) Have all the major use cases and corner cases been considered?
5) Is there any scope for genralising components of the test into the test_utils library?
6) Could the tests use existing components inside the test_utils library?
7) Do the unit tests all pass?
8) Could the code being tested be simplified (design/interfaces etc) to improve testing?
9) Does valgrind report any errors running the unit tests (i.e verify there are no memory leaks)?

== Looking over the code
1) does the code conform to our style guidelines
  > adequate documentaion
  > indentation
  > poor or dangerous constructs
  > includes correctly placed in header or cpp file?
  > is the header file readable, in our standard format and are there adequate comments in the code
  > variable, method and class names conform

2) look out for potential problems
  > is the design modular and scalable? Does it make sense to refactor this?
  > can you see any obvious bugs? Why was this not caught buy the unit test?

3) Packaging
  > is this code in the right place? Should it be broken out in to its own module or merged with a different one?
  > check for circular dependencies between subpackages
