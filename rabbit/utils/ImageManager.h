/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_RABBIT_UTILS_IMAGEMANAGER_H
#define SKA_RABBIT_UTILS_IMAGEMANAGER_H

#include <string>
#include <map>
#include <vector>
#include <memory>
#include <fstream>

struct _cl_device_id;
typedef struct _cl_device_id* cl_device_id;

namespace ska {
namespace rabbit {
namespace utils {

/**
 * @brief
 * @details
 */

class ImageManager
{
    public:
        ImageManager(std::string const& module_root_path);
        ImageManager(ImageManager&);
        ImageManager(ImageManager&&);

        /**
         * @brief return the full path to the image for a specific board
         */
        std::string image_path(std::string const& board_type) const;

        /**
         * @brief return the full path to the image for the specified OpenCL device id
         * @details this can throw if rabbit is not compiled with -DENABLE_OPENCL=true
         */
        std::string image_path(cl_device_id device_id) const;

        /**
         * @brief register an image
         */
        template<typename T> void add(T);

        /**
         * @brief return a list of available board image names
         */
        std::vector<std::string> images() const;

        /**
         * @brief flash an FPGA image from a file to an FPGA
         * @details the image must be an .rbf file written in VHDL, and the FPGA
         *          device is the chip itself, accessible at, e.g., /dev/altera_cvp
         */
        bool flash_fpga_image(std::string const& image_file, std::string const& fpga_device);

    private:
        std::string _path;
        std::unique_ptr<char*> _image_buffer;
        std::ifstream _image_file_stream;
        // We need to use a C-style file pointer here, as using a file
        // stream for the FPGA causes a panic when the stream is closed.
        FILE* _fpga_file_descriptor;

        typedef std::map<std::string, std::string> MapType;
        MapType _images;

};

template<typename T>
void ImageManager::add(T t)
{
    _images.insert(std::make_pair(t.board(), t.image_file()));
}

} // namespace utils
} // namespace rabbit
} // namespace ska

#endif // SKA_RABBIT_UTILS_IMAGEMANAGER_H
