/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "../ImageManager.h"
#include "../OpenCl.h"

#include <exception>
#include <stdexcept>

namespace ska {
namespace rabbit {
namespace utils {

ImageManager::ImageManager(std::string const& module_root_path)
    : _path(module_root_path)
{
}

ImageManager::ImageManager(ImageManager& other)
    : _path(other._path)
    , _image_buffer(std::move(other._image_buffer))
    , _image_file_stream(std::move(other._image_file_stream))
{
}

ImageManager::ImageManager(ImageManager&& other)
    : _path(other._path)
    , _image_buffer(std::move(other._image_buffer))
    , _image_file_stream(std::move(other._image_file_stream))
{
}

std::string ImageManager::image_path(std::string const& board_type) const
{
    MapType::const_iterator it = _images.find(board_type);
    if(it == _images.end())
    {
        throw std::runtime_error("No suitable FPGA image found:" + board_type);
    }
    return _path + it->second;
}

std::vector<std::string> ImageManager::images() const
{
    std::vector<std::string> boards;
    boards.reserve(_images.size());
    for(auto const& image : _images)
    {
        boards.push_back(image.first);
    }
    return boards;
}

bool ImageManager::flash_fpga_image( std::string const& image_file
                                   , std::string const& fpga_device
                                   )
{

    _image_buffer = std::make_unique<char*>(new char[4096]);

    // Set exceptions for the input stream. This implicitly clears the stream's
    // error state flags, so clear() does not need to be called before this.
    _image_file_stream.exceptions(std::ifstream::badbit);

    // Open both input stream and output file. As we are using RAII, these will
    // be closed when this function returns, so we do not manually close them.
    _image_file_stream.open(image_file, std::fstream::in | std::fstream::binary);
    if(!_image_file_stream.is_open())
    {
        throw std::runtime_error("failed to open input file:" + image_file);
        return false;
    }
    // Open the output file (really the FPGA itself) in read and write
    // mode. Simply opening the file in output-only mode causes a panic.
    _fpga_file_descriptor = fopen(fpga_device.c_str(), "wb");
    if(_fpga_file_descriptor == NULL)
    {
        throw std::runtime_error("failed to open output file:" + fpga_device);
        return false;
    }

    // Continuously read chunks of 4096 bytes from input file
    while(_image_file_stream.read(*_image_buffer, 4096))
    {
        // Write 4096 bytes to the output device as were read from the input file
        std::ignore = fwrite(*_image_buffer, sizeof(char), 4096, _fpga_file_descriptor);
    }

    // When there are less than 4096 bytes left to read from the input file, the
    // above while loop will exit, as the read() will fail. The read(), however,
    // will store the number of bytes it was able to read into the buffer, i.e.
    // the number of bytes remaining, which is accessible via the gcount() method.
    if(_image_file_stream.gcount())
    {
        std::ignore = fwrite(*_image_buffer, sizeof(char), _image_file_stream.gcount(), _fpga_file_descriptor);
    }

    return true;

}

#ifdef ENABLE_OPENCL
std::string ImageManager::image_path(cl_device_id device_id) const
{
    std::string board_name;
    char device_name[1024], device_version[1024];

    clGetDeviceInfo(const_cast<cl_device_id>(device_id)
                    , CL_DEVICE_NAME
                    , sizeof(device_name)
                    , device_name
                    , NULL
                   );
    clGetDeviceInfo(const_cast<cl_device_id>(device_id)
                    , CL_DEVICE_VERSION
                    , sizeof(device_version)
                    , device_version
                    , NULL
                   );

    // Add any new boards here
    if(std::string("Intel(R) FPGA Emulation Device") == device_name
       || std::string("EmulatorDevice : Emulated Device") == device_name
      )
    {
        board_name = "emulator";
    }
    else if(std::string("a10pl4_dd4gb_gx115 : A10PL4 (acla10pl40)") == device_name)
    {
        board_name = "a10pl4_dd4gb_gx115";
    }

    return image_path(board_name);
}
#else // ENABLE_OPENCL
std::string ImageManager::image_path(cl_device_id) const
{
    throw std::runtime_error("rabbit not compiled with -DENABLE_OPENCL=true. Request for OpenCL device-specific image failed.");
    return "";
}
#endif // ENABLE_OPENCL

} // namespace utils
} // namespace rabbit
} // namespace ska
