/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "rabbit/fdas/Fdas.h"
#include "rabbit/utils/ImageManager.h"
#include "rabbit/fdas/test/FdasImageManagerTest.h"

#include "panda/test/gtest.h"

namespace ska {
namespace rabbit {
namespace fdas {
namespace test {

ImageManagerTest::ImageManagerTest()
    : ::testing::Test()
{
}

ImageManagerTest::~ImageManagerTest()
{
}

void ImageManagerTest::SetUp()
{
}

void ImageManagerTest::TearDown()
{
}

TEST_F(ImageManagerTest, test_image_manager_fdas)
{
    ska::rabbit::fdas::Fdas fdas;
    auto const& image_manager = fdas.image_manager();
    for(auto const& image : image_manager.images())
    {
        std::string filename = image_manager.image_path(image);
        ASSERT_FALSE(filename.empty());
    }
}

#ifdef ENABLE_INTEL_FPGA
TEST_F(ImageManagerTest, test_flash_fpga_fdas)
{
    ska::rabbit::fdas::Fdas fdas;
    auto & image_manager = fdas.image_manager();
    std::string rbf_file{ska::panda::test::test_file("FDAS_TOP_20240701.rbf")};
    ASSERT_TRUE(image_manager.flash_fpga_image(rbf_file, std::string("/dev/altera_cvp")));
}
#endif // ENABLE_INTEL_FPGA

} // namespace test
} // namespace fdas
} // namespace rabbit
} // namespace ska
