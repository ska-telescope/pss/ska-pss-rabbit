/*
 * FDAS -- Fourier Domain Acceleration Search, FPGA-accelerated with OpenCL
 * Copyright (C) 2020  Parallel and Reconfigurable Computing Lab,
 *                     Dept. of Electrical, Computer, and Software Engineering,
 *                     University of Auckland, New Zealand
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtest/gtest.h>
#include <rabbit/fdas/opencl/FdasSettings.h>

namespace ska {
namespace rabbit {
namespace fdas {
namespace test {

struct FdasSettingsTest : ::testing::Test {
};

TEST_F(FdasSettingsTest, test_invariants)
{
    ska::rabbit::fdas::opencl::FdasSettings settings;

    ASSERT_EQ(settings.n_templates(), 2 * settings.n_templates_per_acceleration_sign() + 1);
    ASSERT_EQ(settings.fft_n_points_per_terminal(), settings.fft_n_points() / settings.fft_n_parallel());
    ASSERT_EQ(settings.fft_n_points(), settings.ftc_tile_size());
    ASSERT_EQ(settings.ftc_tile_payload() + settings.ftc_tile_overlap(), settings.ftc_tile_size());
}

TEST_F(FdasSettingsTest, test_unpacking)
{
    ska::rabbit::fdas::opencl::FdasSettings settings;
    ska::rabbit::fdas::opencl::FdasSettings::candidate_location_t location = 0U;

    unsigned int harm_ref = 5, harm;
    int tmpl_ref = -17, tmpl;
    unsigned int freq_ref = 12345, freq;

    location |= ((harm_ref - 1) & 0x7) << 29;
    location |= ((tmpl_ref + settings.n_templates_per_acceleration_sign()) & 0x7f) << 22;
    location |= freq_ref & 0x3fffff;
    settings.hms_unpack_location(location, harm, tmpl, freq);

    ASSERT_EQ(harm, harm_ref);
    ASSERT_EQ(tmpl, tmpl_ref);
    ASSERT_EQ(freq, freq_ref);
}

} // namespace test
} // namespace fdas
} // namespace rabbit
} // namespace ska
