/*
 * FDAS -- Fourier Domain Acceleration Search, FPGA-accelerated with OpenCL
 * Copyright (C) 2020  Parallel and Reconfigurable Computing Lab,
 *                     Dept. of Electrical, Computer, and Software Engineering,
 *                     University of Auckland, New Zealand
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "rabbit/fdas/opencl/FdasSettings.h"
#include "rabbit/fdas/opencl/src/fdas_config.h"

namespace ska {
namespace rabbit {
namespace fdas {
namespace opencl {

unsigned int FdasSettings::n_templates() const {
    return N_TEMPLATES;
}

unsigned int FdasSettings::n_templates_per_acceleration_sign() const {
    return N_TMPL_PER_ACCEL_SIGN;
}

unsigned int FdasSettings::max_template_length() const {
    return MAX_TEMPLATE_LEN;
}

unsigned int FdasSettings::fft_n_points() const {
    return FFT_N_POINTS;
}

unsigned int FdasSettings::fft_n_parallel() const {
    return FFT_N_PARALLEL;
}

unsigned int FdasSettings::fft_n_points_per_terminal() const {
    return FFT_N_POINTS_PER_TERMINAL;
}

unsigned int FdasSettings::ftc_tile_size() const {
    return FTC_TILE_SZ;
}

unsigned int FdasSettings::ftc_tile_overlap() const {
    return FTC_TILE_OVERLAP;
}

unsigned int FdasSettings::ftc_tile_payload() const {
    return FTC_TILE_PAYLOAD;
}

unsigned int FdasSettings::ftc_group_size() const {
    return FTC_GROUP_SZ;
}

unsigned int FdasSettings::hms_n_planes() const {
    return HMS_N_PLANES;
}

bool FdasSettings::hms_store_planes() const {
    return HMS_STORE_PLANES;
}

FdasSettings::candidate_location_t FdasSettings::hms_invalid_location() const {
    return HMS_INVALID_LOCATION;
}

void FdasSettings::hms_unpack_location(FdasSettings::candidate_location_t location,
                                       unsigned int &harmonic, int &template_num, unsigned int &frequency_bin) const {
    harmonic = ((location >> 29) & 0x7) + 1;
    template_num = ((location >> 22) & 0x7f) - (int) N_TMPL_PER_ACCEL_SIGN;
    frequency_bin = location & 0x3fffff;
}

unsigned int FdasSettings::n_candidates() const {
    return N_CANDIDATES;
}

} // namespace opencl
} // namespace fdas
} // namespace rabbit
} // namespace ska
