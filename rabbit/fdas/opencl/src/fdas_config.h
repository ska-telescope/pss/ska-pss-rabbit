/*
 * FDAS -- Fourier Domain Acceleration Search, FPGA-accelerated with OpenCL
 * Copyright (C) 2020  Parallel and Reconfigurable Computing Lab,
 *                     Dept. of Electrical, Computer, and Software Engineering,
 *                     University of Auckland, New Zealand
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SKA_RABBIT_FDAS_OPENCL_FDAS_CONFIG_H
#define SKA_RABBIT_FDAS_OPENCL_FDAS_CONFIG_H

/*
 *   _____ ____    _    ____                     __ _                       _   _
 *  |  ___|  _ \  / \  / ___|    ___ ___  _ __  / _(_) __ _ _   _ _ __ __ _| |_(_) ___  _ __
 *  | |_  | | | |/ _ \ \___ \   / __/ _ \| '_ \| |_| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \
 *  |  _| | |_| / ___ \ ___) | | (_| (_) | | | |  _| | (_| | |_| | | | (_| | |_| | (_) | | | |
 *  |_|   |____/_/   \_\____/   \___\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
 *                                                    |___/
 */
// === Inputs ==========================================================================================================

// The templates correspond to different accelerations to test for. There are two equally-sized sets of templates which
// only differ in the sign of the acceleration. Additionally, one template, located between the groups, just forwards
// the input signal.
#define N_TMPL_PER_ACCEL_SIGN          (42)
#define N_TEMPLATES                    (N_TMPL_PER_ACCEL_SIGN + 1 + N_TMPL_PER_ACCEL_SIGN)

// Maximum length (= number of coefficients) across all templates
#define MAX_TEMPLATE_LEN               (421)

// === FFT engine configuration ========================================================================================

// Size of the transform -- must be a compile-time constant. The maximum size currently supported by the engine is 2^12;
// larger transforms would require additional precomputed twiddle factors.
#define FFT_N_POINTS_LOG               (11)
#define FFT_N_POINTS                   (1 << FFT_N_POINTS_LOG)

// The engine is __hard-wired__ to processes 4 points in parallel. DO NOT CHANGE unless you also provide a suitable,
// alternative engine implementation. This requires structural modifications that go beyond the scope of a few #defines.
#define FFT_N_PARALLEL_LOG             (2)
#define FFT_N_PARALLEL                 (1 << FFT_N_PARALLEL_LOG)

// Number of points that arrive at each input terminal
#define FFT_N_POINTS_PER_TERMINAL_LOG  (FFT_N_POINTS_LOG - FFT_N_PARALLEL_LOG)
#define FFT_N_POINTS_PER_TERMINAL      (1 << FFT_N_POINTS_PER_TERMINAL_LOG)

// === FT convolution implementation with overlap-save algorithm =======================================================

// The tile size for the overlap-save algorithm must match the FFT size
#define FTC_TILE_SZ                    (FFT_N_POINTS)

// Amount of overlap between neighbouring tiles (also, zero-padding for the first tile). Only the unique parts will be
// used later on.
#define FTC_TILE_OVERLAP               (MAX_TEMPLATE_LEN - 1)
#define FTC_TILE_PAYLOAD               (FTC_TILE_SZ - FTC_TILE_OVERLAP)

// Set how many templates are processed in parallel
#define FTC_GROUP_SZ                   (5)

// === Harmonic summing ================================================================================================

// Number of harmonic planes to consider, i.e. FOP = HP_1, HP_2, ..., HP_{HMS_N_PLANES}
#define HMS_N_PLANES                   (8)

// Maximum number of detections, i.e. pulsar candidates, recorded per harmonic plane
#define HMS_DETECTION_SZ               (64)

// If true, explicitly write harmonic planes to memory, otherwise compare FOP values to thresholds on-the-fly
#define HMS_STORE_PLANES               (false)

// Format used to encode a detection location in a 32-bit unsigned integer:
//   ┌───┬───────┬──────────────────────┐
//   │ k │templ. │    frequency bin     │
//   └───┴───────┴──────────────────────┘
//  31 29|28   22|21                    0
#define HMS_ENCODE_LOCATION(harm, tmpl, freq) \
    ((((harm - 1) & 0x7) << 29) | (((tmpl + N_TMPL_PER_ACCEL_SIGN) & 0x7f) << 22) | (freq & 0x3fffff))
#define HMS_INVALID_LOCATION           (HMS_ENCODE_LOCATION(1, N_TMPL_PER_ACCEL_SIGN + 1, 0))

// Parallelisation factor in the HMS_UNROLL approach. Always change both macros!
#define HMS_X                          (4)
#define HMS_CHANNEL_LOOP_UNROLL        _Pragma("unroll 4")

// === Output ==========================================================================================================

// Maximum number of pulsar candidates returned from the FDAS module
#define N_CANDIDATES                   (HMS_N_PLANES * HMS_DETECTION_SZ)

#endif // SKA_RABBIT_FDAS_OPENCL_FDAS_CONFIG_H
