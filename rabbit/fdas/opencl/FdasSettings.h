/*
 * FDAS -- Fourier Domain Acceleration Search, FPGA-accelerated with OpenCL
 * Copyright (C) 2020  Parallel and Reconfigurable Computing Lab,
 *                     Dept. of Electrical, Computer, and Software Engineering,
 *                     University of Auckland, New Zealand
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SKA_RABBIT_FDAS_OPENCL_FDAS_SETTINGS_H
#define SKA_RABBIT_FDAS_OPENCL_FDAS_SETTINGS_H

#include <cstdint>

namespace ska {
namespace rabbit {
namespace fdas {
namespace opencl {

/**
 * @brief Interface for accessing compile-time settings of the FDAS OpenCL implementation
 */
class FdasSettings {
public:
    /**
     * @brief Data format used to encode pulsar candidate locations
     * @verbatim
     *   ┌───┬───────┬──────────────────────┐
     *   │ k │templ. │    frequency bin     │
     *   └───┴───────┴──────────────────────┘
     *  31 29|28   22|21                    0
     * @endverbatim
     */
    typedef uint32_t candidate_location_t;

public:
    FdasSettings() = default;
    ~FdasSettings() = default;

    /**
     * @return the total number of templates
     * @see FdasSettings::n_templates_per_acceleration_sign
     */
    unsigned int n_templates() const;
    /**
     * The templates correspond to different accelerations to test for. There are two equally-sized sets of templates which
     * only differ in the sign of the acceleration. Additionally, one template, located between the groups, just forwards
     * the input signal.
     * @return the number of templates that represent positive (or negative) accelerations
     */
    unsigned int n_templates_per_acceleration_sign() const;
    /**
     * @return the maximum length (= number of coefficients) across all templates
     */
    unsigned int max_template_length() const;

    /**
     * @return the size of Fourier transform performed by the FFT engines
     */
    unsigned int fft_n_points() const;
    /**
     * @return the number of points processed in parallel by the FFT engines
     */
    unsigned int fft_n_parallel() const;
    /**
     * @return the number of points that arrive at each engine terminal
     */
    unsigned int fft_n_points_per_terminal() const;

    /**
     * @return the tile size for the overlap-save algorithm
     */
    unsigned int ftc_tile_size() const;
    /**
     * @return the amount of overlap between neighbouring tiles (also, zero-padding for the first tile)
     */
    unsigned int ftc_tile_overlap() const;
    /**
     * @return the payload (i.e. points that are not discarded later) of each tile
     */
    unsigned int ftc_tile_payload() const;
    /**
     * @return the number of templates that are convolved in parallel
     */
    unsigned int ftc_group_size() const;

    /**
     * @return the number of harmonic planes to consider, i.e. FOP = HP_1, HP_2, ..., HP_{hms_n_planes}
     */
    unsigned int hms_n_planes() const;
    /**
     * @return true if all harmonic planes are stored to global memory. ONLY FOR DEBUGGING
     */
    bool hms_store_planes() const;

    /**
     * @return magic number indicating an invalid entry in the candidate lists
     */
    candidate_location_t hms_invalid_location() const;
    /**
     * Unpacks a candidate location into its components
     * @param location the location to unpack
     * @param harmonic the variable to store the location's harmonic number in
     * @param template_num the variable to store location's template number in
     * @param frequency_bin the variable to store the location's frequency bin in
     */
    void hms_unpack_location(candidate_location_t location,
                             unsigned int &harmonic, int &template_num, unsigned int &frequency_bin) const;

    /**
     * The FDAS module always returns fixed-sized candidate lists. "Empty" slots use the value returned
     * by hms_invalid_location() as their location.
     *
     * @see FdasSettings::hms_invalid_location()
     *
     * @return the number of candidates
     */
    unsigned int n_candidates() const;
};

} // namespace opencl
} // namespace fdas
} // namespace rabbit
} // namespace ska

#endif // SKA_RABBIT_FDAS_OPENCL_FDAS_SETTINGS_H
