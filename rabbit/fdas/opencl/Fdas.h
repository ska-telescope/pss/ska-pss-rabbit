/*
 * FDAS -- Fourier Domain Acceleration Search, FPGA-accelerated with OpenCL
 * Copyright (C) 2020  Parallel and Reconfigurable Computing Lab,
 *                     Dept. of Electrical, Computer, and Software Engineering,
 *                     University of Auckland, New Zealand
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SKA_RABBIT_FDAS_OPENCL_FDAS_H
#define SKA_RABBIT_FDAS_OPENCL_FDAS_H

/*
 * @brief Tiles and overlaps the input data, and sends bundles of points in the correct format to an FFT engine
 * @param input a global memory buffer holding the zero-padded input data
 */
kernel void tile_input(global volatile float2 * restrict input);

/*
 * @brief Performs a forward FFT
 * @param n_tiles the number of tiles to transform
 */
kernel void fft(const uint n_tiles);

/*
 * @brief Receives bundles of Fourier-transformed points and stores them in global memory
 * @param tiles the global memory buffer to store the FT tiles into
 */
kernel void store_tiles(global float2 * restrict tiles);

/*
 * @brief Multiplies FT input points with FT template coefficients, and forwards the result to an iFFT engine
 * @param tiles a global memory buffer containing the tiled/overlapped and Fourier-transformed input spectrum
 * @param templates a global memory buffer containing Fourier-transformed template coefficients
 */
kernel void mux_and_mult(global volatile float2 * restrict tiles,
                         global volatile float2 * restrict templates);

/*
 * @brief Performs an inverse FFT
 *
 * Note: There is a total number of FdasSettings::ftc_group_size() of iFFT kernels, i.e. ifft_0, ifft_1, ...
 *
 * @param n_tiles the number of tiles to transform
 */
kernel void ifft_0(const uint n_tiles);

/*
 * @brief Receives the results of the iFFT engines, computes the normalised power of each point, and writes the FOP
 * @param fop the global memory buffer to store the filter-output plane in
 * @param n_frequency_bins the size of the input spectrum, and thus, the size of each FOP row
 */
kernel void square_and_discard(global float * restrict fop,
                               const uint n_frequency_bins);

/*
 * @brief Performs harmonic summing and candidate detection
 * @param fop the global memory buffer containing the filter-output plane computed by a previous FT convolution step
 * @param n_frequency_bins the size of the input spectrum, and thus, the size of each FOP row
 * @param thresholds a global memory buffer containing the detection threshold for each harmonic plane
 * @param detection_location a global memory buffer which stores the locations of pulsar candidates
 * @param detection_power a global memory buffer which stores the power of potential detections
 */
kernel void harmonic_summing(global volatile float * restrict fop,
                             const uint n_frequency_bins,
                             global float * restrict thresholds,
                             global uint * restrict detection_location,
                             global float * restrict detection_power);

#endif // SKA_RABBIT_FDAS_OPENCL_FDAS_SETTINGS_H
