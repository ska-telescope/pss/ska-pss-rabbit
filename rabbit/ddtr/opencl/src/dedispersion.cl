//#include "rabbit/ddtr/opencl/Ddtr.h"
#include "rabbit/ddtr/opencl/kernel_params.h"
#include "rabbit/ddtr/opencl/input_params.h"

//__attribute__((task))
kernel void dedisperse (
    global float* restrict dmshift_buffer1,
    global float* restrict dmshift_buffer2,
    global unsigned char* restrict input_buffer,
    global unsigned int* restrict output_buffer, int dm1, int dm2, int dsamp
)
{
    //load constant dmshift values for each channel from global memory
    float dmshifts1[(NUM_CHANNELS/NUM_STORED_CHANNELS)][NUM_STORED_CHANNELS];
    float dmshifts2[(NUM_CHANNELS/NUM_STORED_CHANNELS)][NUM_STORED_CHANNELS];
    private float dmshift_line1[NUM_STORED_CHANNELS];
    private float dmshift_line2[NUM_STORED_CHANNELS];
    for (short i = 0, c = 0; c < (NUM_CHANNELS/NUM_STORED_CHANNELS);) {
        #pragma unroll
        for (int k = (NUM_STORED_CHANNELS-1); k > 0; k--) {
            dmshift_line1[k] = dmshift_line1[k-1];
            dmshift_line2[k] = dmshift_line2[k-1];
        }
        dmshift_line1[0] = dmshift_buffer1[(c << LOG2_NUM_STORED_CHANNELS) + i];
        dmshift_line2[0] = dmshift_buffer2[(c << LOG2_NUM_STORED_CHANNELS) + i];

        if (i == NUM_STORED_CHANNELS - 1) {
            #pragma unroll
            for (int ci = 0; ci < NUM_STORED_CHANNELS; ci++) {
                dmshifts1[c][ci] = dmshift_line1[(NUM_STORED_CHANNELS - 1) - ci];
                dmshifts2[c][ci] = dmshift_line2[(NUM_STORED_CHANNELS - 1) - ci];
            }
        }

        i++;
        if (i == NUM_STORED_CHANNELS) {
            i = 0;
            c++;
        }
    }

    unsigned char sample_buffer[(NUM_STORED_SAMPLES/NUM_REGS)][NUM_STORED_CHANNELS][NUM_REGS]; //rotating buffer
    private unsigned char sample_line_fill[NUM_STORED_CHANNELS*NUM_REGS];

    //initial sample_buffer fill
    for (short i = 0, si = 0; si < (NUM_STORED_SAMPLES/NUM_REGS);) {
        #pragma unroll
        for (int k = ((NUM_STORED_CHANNELS*NUM_REGS)-1); k > 0; k--) {
            sample_line_fill[k] = sample_line_fill[k-1];
        }
        sample_line_fill[0] = input_buffer[(si << LOG2_NUM_STORED_CHANNELS_x_NUM_REGS) + i];

        if (i == NUM_STORED_CHANNELS * NUM_REGS - 1) {
            #pragma unroll
            for (int ci = 0; ci < NUM_STORED_CHANNELS; ci++) {
                #pragma unroll
                for (int j = 0; j < NUM_REGS; j++) {
                    sample_buffer[si][ci][j] = sample_line_fill[((NUM_STORED_CHANNELS*NUM_REGS) - 1) - ((ci << LOG2_NUM_REGS) + j)];
                }
            }
        }

        i++;
        if (i == NUM_STORED_CHANNELS * NUM_REGS) {
            i = 0;
            si++;
        }
    }

    int next_input_sample_index = NUM_STORED_SAMPLES*NUM_STORED_CHANNELS;

    for (int c = 0; c < (NUM_CHANNELS/NUM_STORED_CHANNELS); c++) {

        //sample_block usage hard coded to 16384 NUM_STORED_SAMPLES
        for (int sample_block = 0; sample_block < (NUM_SAMPLES/NUM_REGS); sample_block++) {

            private unsigned char sample_line[NUM_STORED_CHANNELS*NUM_REGS];

            for (short d = 0; (d & 0xffff) < TDMS; d++) {

                //add samples from all NUM_STORED_CHANNELS channels for the next NUM_REGS time steps in an adder tree
                //    -> (NUM_STORED_CHANNELS x NUM_REGS) additions
                unsigned int shuffle[LOG2_NUM_REGS + 1][NUM_REGS];
                unsigned int acc[NUM_REGS];

                #pragma unroll
                for (int j = 0; j < NUM_REGS; j++) {
                    acc[j] = 0;
                }

                float dm_step = (d & 0xffff);// * (TDMS_STEP * TSAMP_INV); //do constant multiplication on host
                int shift;
                #pragma unroll
                for (int sc = 0; sc < NUM_STORED_CHANNELS; sc++) {
                    if((d & 0xffff) < TDMS/2){
                    shift = (int) ((dm_step + dm1) * dmshifts1[c][sc]);
                    }
                    else {
                    shift = (int) ((dm_step + dm2) * dmshifts2[c][sc]);
                    }

                    #pragma unroll
                    for (int j = 0; j < NUM_REGS; j++) {
                        shuffle[0][j] = sample_buffer[((sample_block + ((shift + (NUM_REGS - 1) - j) >> LOG2_NUM_REGS)) & 0x7FF)][sc][j];
                    }

                    #pragma unroll
                    for (int shift_lvl = 0; shift_lvl < LOG2_NUM_REGS; shift_lvl++) {
                        #pragma unroll
                        for (int j = 0; j < NUM_REGS; j++) {
                            shuffle[shift_lvl + 1][j] = ((shift >> j) & 0x1) ? shuffle[shift_lvl][(j + (1 << shift_lvl)) & 0x7] : shuffle[shift_lvl][j];
                        }
                    }

                    #pragma unroll
                    for (int j = 0; j < NUM_REGS; j++) {
                        acc[j] += shuffle[LOG2_NUM_REGS][j];
                    }
                }

                // down-sampling added here
                #pragma unroll
                for (int j = 0; j < NUM_REGS; j = j + 4)
                {
                                           if (dsamp == 2)
                    {
                    acc[j] = (acc[j] + acc[j+1]) >> LOG2_DSAMP2;
                    acc[j+2] = (acc[j+2] + acc[j+3]) >> LOG2_DSAMP2;
                    }
                    if (dsamp == 4)
                    {
                    acc[j] = (acc[j] + acc[j+1] + acc[j+2] + acc[j+3]) >> LOG2_DSAMP4;
                    }
                }

                #pragma unroll
                // for (int j = 0; j < NUM_REGS; j=j+4)
                for (int j = 0; j < NUM_REGS; j++)
                output_buffer[(sample_block * TDMS_x_NUM_REGS) + ((d & 0xffff) << LOG2_NUM_REGS) + j] = output_buffer[(sample_block * TDMS_x_NUM_REGS) + ((d & 0xffff) << LOG2_NUM_REGS) + j] + acc[j];

                // if (dsamp == 2)
                // {
                // #pragma unroll
                // for (int j = 0; j < NUM_REGS; j=j+4)
                // output_buffer[(sample_block * TDMS_x_NUM_REGS) + ((d & 0xffff) << LOG2_NUM_REGS) + j+2] = acc[j+2];
                // }

                // if (dsamp == 1)
                // {
                // #pragma unroll
                // for (int j = 0; j < NUM_REGS; j=j+2)
                // output_buffer[(sample_block * TDMS_x_NUM_REGS) + ((d & 0xffff) << LOG2_NUM_REGS) + j+1] = acc[j+1];
                // }

                //start filling shift register at beginning of loop, then spin the data around the shift register
                unsigned char next_sample = ((d & 0xffff) < (NUM_STORED_CHANNELS*NUM_REGS)) ? input_buffer[next_input_sample_index + (d & 0xffff)] : sample_line[(NUM_STORED_CHANNELS*NUM_REGS)-1];
                #pragma unroll
                for (int k = (NUM_STORED_CHANNELS*NUM_REGS)-1; k > 0; k--) {
                    sample_line[k] = sample_line[k-1];
                }
                sample_line[0] = next_sample;

                //load shift register into local memory on last iteration
                if ((d & 0xffff) == (TDMS - 1)) {
                    #pragma unroll
                    for (int ci = 0; ci < NUM_STORED_CHANNELS; ci++) {
                        #pragma unroll
                        for (int j = 0; j < NUM_REGS; j++) {
                            sample_buffer[sample_block & 0x7FF][ci][j] = sample_line[((NUM_STORED_CHANNELS*NUM_REGS) - 1) - (((ci << LOG2_NUM_REGS) + j + (TDMS & NUM_STORED_CHANNELS_x_NUM_REGS_MASK)) & NUM_STORED_CHANNELS_x_NUM_REGS_MASK)]; //compensate for possible over-shift if TDMS is not divisible by (NUM_STORED_CHANNELS*NUM_REGS)
                        }
                    }

                    next_input_sample_index += (NUM_STORED_CHANNELS*NUM_REGS);
                }

            }
        }
    }
}

