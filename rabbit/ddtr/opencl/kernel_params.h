
//the total number of parallel additions performed by the kernel below is NUM_STORED_CHANNELS x NUM_REGS

//number of sequential time steps to accumulate from in parallel
#define NUM_REGS 16 
#define LOG2_NUM_REGS 4

//sample window = NUM_STORED_CHANNELS x NUM_STORED_SAMPLES
#define NUM_STORED_SAMPLES 16384 //plenty of room for (max shift + NUM_REGS) + delays on filling the rotating buffer - NUM_STORED_SAMPLES / NUM_REGS must be an integer, NUM_SAMPLES / NUM_STORED_SAMPLES must be an integer
#define NUM_STORED_CHANNELS 64 //parallel channels - NUM_CHANNELS / NUM_STORED_CHANNELS must be an integer
#define LOG2_NUM_STORED_CHANNELS 6

#define LOG2_NUM_STORED_CHANNELS_x_NUM_REGS (LOG2_NUM_REGS + LOG2_NUM_STORED_CHANNELS)

//mask LOG2_NUM_STORED_CHANNELS_x_NUM_REGS bits
#define NUM_STORED_CHANNELS_x_NUM_REGS_MASK 0x3FF

#define LOG2_DSAMP2 1
#define LOG2_DSAMP4 2

#define TDMS_x_NUM_REGS (TDMS * NUM_REGS)
