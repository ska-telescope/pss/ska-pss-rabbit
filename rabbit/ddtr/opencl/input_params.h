

#define NUM_CHANNELS 4096
#define NUM_SAMPLES 98304
#define TDMS 1000 //must be greater NUM_STORED_CHANNELS * NUM_REGS
#define TDMS_STEP 0.02f
#define DSAMP 1
#define STEP 10

#define TSAMP  0.00024576f
#define TSAMP_INV (1/TSAMP)

