/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_RABBIT_CXFT_OPENCL_EIGHT_MILLION_POINT_CXFTPLAN_H
#define SKA_RABBIT_CXFT_OPENCL_EIGHT_MILLION_POINT_CXFTPLAN_H

#include <boost/align/aligned_allocator.hpp>
#include <complex>
#include <vector>

namespace ska {
namespace rabbit {
namespace cxft {
namespace opencl {
namespace eight_million_point {

class Cxft;

/**
 * @brief
 * @details
 */

class CxftPlan
{
    public:
        typedef std::vector<std::complex<float>, boost::alignment::aligned_allocator<std::complex<float>, 64>> Twiddles;

    public:
        CxftPlan(Cxft const& cxft, size_t fft_size);

        ~CxftPlan();

        bool valid(size_t fft_size) const;

        Twiddles const& twiddles() const;

    private:
        Twiddles _twiddles;
};


} // namespace eight_million_point
} // namespace opencl
} // namespace cxft
} // namespace rabbit
} // namespace ska

#endif // SKA_RABBIT_CXFT_OPENCL_EIGHT_MILLION_POINT_CXFTPLAN_H
