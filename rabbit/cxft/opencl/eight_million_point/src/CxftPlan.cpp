/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "rabbit/cxft/opencl/eight_million_point/CxftPlan.h"
#include "rabbit/cxft/opencl/eight_million_point/Cxft.h"


namespace ska {
namespace rabbit {
namespace cxft {
namespace opencl {
namespace eight_million_point {

CxftPlan::CxftPlan(Cxft const& cxft, size_t fft_size)
    : _twiddles(fft_size)
{
    size_t step_size=cxft.max_size()/fft_size;
    size_t index=0;
    for (unsigned int i=0; i<fft_size; ++i)
    {
        _twiddles[i]= cxft._twiddles[index];
        index+=step_size;
    }
}

CxftPlan::~CxftPlan()
{
}

bool CxftPlan::valid(size_t fft_size) const
{
    return fft_size==_twiddles.size();
}

typename CxftPlan::Twiddles const& CxftPlan::twiddles() const
{
    return _twiddles;
}

} // namespace eight_million_point
} // namespace opencl
} // namespace cxft
} // namespace rabbit
} // namespace ska
