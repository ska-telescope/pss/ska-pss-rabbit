/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "rabbit/cxft/opencl/eight_million_point/Cxft.h"
#include "rabbit/cxft/opencl/eight_million_point/CxftPlan.h"
#include "rabbit/cxft/opencl/eight_million_point/FftConfig.h"
#include "rabbit/cxft/opencl/eight_million_point/CxftEightMillionPointImageManager.h"
#include <cmath>

namespace ska {
namespace rabbit {
namespace cxft {
namespace opencl {
namespace eight_million_point {


Cxft::Cxft()
    : _twiddles(MAX_SIZE)
{
    for (unsigned int i=0; i<MAX_SIZE; ++i)
    {
        double k = -2*M_PI/MAX_SIZE;
        (_twiddles.begin()+i)->real(cos(i*k));
        (_twiddles.begin()+i)->imag(sin(i*k));
    }
}

Cxft::~Cxft()
{
}

CxftPlan Cxft::plan(size_t fft_size) const
{
    return CxftPlan(*this, fft_size);
}

utils::ImageManager& Cxft::image_manager() const
{
    return cxft::opencl::eight_million_point::image_manager();
}

typename Cxft::Twiddles const& Cxft::twiddles() const
{
    return _twiddles;
}

std::size_t Cxft::max_size()
{
    return MAX_SIZE;
}

std::size_t Cxft::min_size()
{
    return MIN_SIZE;
}

} // namespace eight_million_point
} // namespace opencl
} // namespace cxft
} // namespace rabbit
} // namespace ska
