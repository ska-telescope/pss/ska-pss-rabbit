// Copyright (C) 2013-2020 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

#ifndef SKA_RABBIT_CXFT_OPENCL_8M_KERNEL_CONFIG_H
#define SKA_RABBIT_CXFT_OPENCL_8M_KERNEL_CONFIG_H

#include "FftConfig.h"

/**
 *@details
 *      Main FFT static parameters: LOGN, LOGM
 *      Each value can also be supplied on command line
 *      Determines FFT size of even/odd kernel
 *      Values larger than 3 are legal when using an 8 points engine
 *      Values smaller than 12 use the precomputed twiddle factors
 */
#ifndef LOGN
#  define LOGN (MAX_N-1)/2
#endif // LOGN

#ifndef LOGM
#define LOGM LOGN
#endif // LOGM

#if LOGM>LOGN
#error "LOGN must be the larger dimension i.e. LOGM<=LOGN."
#endif // LOGM>LOGN

/**
 * @details
 *      LOGPOINTS:How many points to process in parallel
 *      Currently supported values are 4 (for 16 points in parallel),
 *      3 (for 8 points in parallel), and
 *      2 (for 4 points in parallel)
 */
#ifndef LOGPOINTS
#define LOGPOINTS 2
#endif // LOGPOINTS

#if LOGPOINTS>=LOGM
#error "LOGPOINTS must be strictly smaller than LOGN,LOGM."
#endif // LOGPOINTS>=LOGM

#define POINTS (1 << LOGPOINTS)
#define N (1 << LOGN)
#define M (1 << LOGM)

#define TYPE float2

/// convenience struct representing the 4 elements processed in parallel
typedef union {
  struct {
    TYPE i0;
    TYPE i1;
    TYPE i2;
    TYPE i3;
  };
  TYPE d[4];
} float2x4;


typedef struct {
    TYPE i0;
    TYPE i1;
    TYPE i2;
} float2x3;

typedef struct {
    TYPE i0;
    TYPE i1;
} float2x2;

/// Convenience struct representing the 8 data points processed each step
/// Each member is a TYPE representing a complex number
typedef union {
  struct {
    TYPE i0;
    TYPE i1;
    TYPE i2;
    TYPE i3;
    TYPE i4;
    TYPE i5;
    TYPE i6;
    TYPE i7;
  };
  TYPE d[8];
} float2x8;


typedef struct {
    TYPE i0;
    TYPE i1;
    TYPE i2;
    TYPE i3;
    TYPE i4;
    TYPE i5;
} float2x6;

/// Convenience struct representing the 8 data points processed each step
/// Each member is a TYPE representing a complex number
typedef union {
  struct {
    TYPE i0;
    TYPE i1;
    TYPE i2;
    TYPE i3;
    TYPE i4;
    TYPE i5;
    TYPE i6;
    TYPE i7;
    TYPE i8;
    TYPE i9;
    TYPE i10;
    TYPE i11;
    TYPE i12;
    TYPE i13;
    TYPE i14;
    TYPE i15;
  };
  TYPE d[16];
} float2x16;

typedef struct {
  TYPE i0;
  TYPE i1;
  TYPE i2;
  TYPE i3;
  TYPE i4;
  TYPE i5;
  TYPE i6;
  TYPE i7;
  TYPE i8;
  TYPE i9;
  TYPE i10;
  TYPE i11;
} float2x12;


#if (POINTS == 16)
  typedef float2x16 float_vector;
  typedef ulong16 ulong_vector;
#define SPLIT_MEM 2
#elif  (POINTS == 8)
  typedef float2x8 float_vector;
  typedef ulong8 ulong_vector;
#define SPLIT_MEM 1
#elif (POINTS == 4)
  typedef float2x4 float_vector;
  typedef ulong4 ulong_vector;
#define SPLIT_MEM 1
#else
  #error "LOGPOINTS must be either 2/3/4 (Only 4/8/16 points in parallel are supported)"
#endif // POINTS & SPLIT_MEM

/// Mask for narrowing down hardware that deals with dynamic rows/columns
#define MAX_LOGN_MASK 0xF
#if (LOGN & MAX_LOGN_MASK) != LOGN
  #error "LOGN is too large. Update internal MAX_LOGN_MASK value".
#endif // MAX_LOGN_MASK

#endif // SKA_RABBIT_CXFT_OPENCL_8M_KERNEL_CONFIG_H
