Eight Million point FFT Intel FPGA

#description
Eight Million point FFT is a derived code from Intel's FFT(1D Off-Chip) design example. FftConfig.h defines the maximum FFT length, e.g. MAX_N=23, MAX_SIZE=2^23 in case of eight million. fft1d kernel computes FFT of even or odd data i.e. four million samples and last_stage kernel computes eight million FFT. This can be configured for different FFT lenght while compilation. MAX_N can be any odd number between 7 to 25.

#external files
Following files have been generated externally by Intel, they are reused from FFT code: https://www.intel.com/content/www/us/en/programmable/support/support-resources/design-examples/design-software/opencl/fft-1d-offchip.html
Perm.h
Twid_4.h
Twid_8.h
Twid_16.h
