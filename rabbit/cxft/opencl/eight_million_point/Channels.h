// Copyright (C) 2013-2020 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

#ifndef SKA_RABBIT_CXFT_OPENCL_8M_CHANNELS_H
#define SKA_RABBIT_CXFT_OPENCL_8M_CHANNELS_H


#include "KernelConfig.h"

/**
 * @brief
 *      defines FIFO buffers write_channel_intel & read_channel_intel
 * @details
 *      Data in the channel is written in FIFO order
 *      Data written to a channel remains in a channel provided that the kernel program remains loaded on FPGA
 *      chan_mwt_in is used in the 'fetch' & 'fft1d' kernels to write the data
 *      chan_mwt_out is used in the 'fft1d' & 'transpose' kernels to read the data
 */
channel float_vector chan_mwt_in[2] __attribute__((depth(64)));
channel float_vector chan_mwt_out[2] __attribute__((depth(64)));

#endif // SKA_RABBIT_CXFT_OPENCL_8M_CHANNELS_H
