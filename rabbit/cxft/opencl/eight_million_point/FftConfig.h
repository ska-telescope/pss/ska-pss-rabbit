// Copyright (C) 2013-2014 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

#ifndef SKA_RABBIT_CXFT_OPENCL_EIGHTM_FFT_CONFIG_H
#define SKA_RABBIT_CXFT_OPENCL_EIGHTM_FFT_CONFIG_H

/**
 * @brief define the maximum and the minimum FFT length
 * @details
 *      Not for exposure outside of the context of kernel compilation.
 *      As the last_stage kernel is additional,
 *      MAX_N or MIN_N is expected to be odd number between 7 to 25, e.g. MAX_N=23 for eight million FFT(2^23)
 *      For example fft1d kernel runs 2's even/odd powered FFT, 1024 (2^10) points, then MAX_N will be 10+1=11
 *      Last stage kernel computes additional (+1 in MAX_N) FFT when even/odd is computed separately.
 */
#ifndef MAX_N
#  define MAX_N 25
#endif // MAX_N

#ifndef MAX_SIZE
#  define MAX_SIZE (1 << MAX_N)
#endif // MAX_SIZE

#ifndef MIN_N
#  define MIN_N 7
#endif // MIN_N

#ifndef MIN_SIZE
#  define MIN_SIZE (1 << MIN_N)
#endif // MIN_SIZE

#endif // SKA_RABBIT_CXFT_OPENCL_EIGHTM_FFT_CONFIG_H
