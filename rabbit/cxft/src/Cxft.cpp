/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "rabbit/cxft/Cxft.h"
#include "rabbit/cxft/CxftImageManager.h"

namespace ska {
namespace rabbit {
namespace cxft {


Cxft::Cxft()
    : _images(cxft::image_manager())
{
}

Cxft::~Cxft()
{
}

utils::ImageManager& Cxft::image_manager()
{
    return _images;
}

#ifdef ENABLE_OPENCL

std::string Cxft::eight_million_point_image(std::string const& board) const
{
    return _opencl.eight_million_point().image_manager().image_path(board);
}

std::string Cxft::eight_million_point_image(cl_device_id device_id) const
{
    return _opencl.eight_million_point().image_manager().image_path(device_id);
}

opencl::eight_million_point::Cxft const& Cxft::eight_million_point() const
{
    return _opencl.eight_million_point();
}

utils::ImageManager& Cxft::eight_million_point_image_manager()
{
    return _opencl.eight_million_point().image_manager();
}

#endif // ENABLE_OPENCL

} // namespace Cxft
} // namespace rabbit
} // namespace ska
