/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "../CxftTest.h"
#include "rabbit/cxft/Cxft.h"
#include "iostream"

namespace ska {
namespace rabbit {
namespace cxft {
namespace test {


CxftTest::CxftTest()
    : ::testing::Test()
{
}

CxftTest::~CxftTest()
{
}

TEST_F(CxftTest, test_image_manager_cxft)
{
    ska::rabbit::cxft::Cxft cxft;
    auto const& image_manager = cxft.image_manager();
    for(auto const& image : image_manager.images())
    {
        std::string filename = image_manager.image_path(image);
        ASSERT_FALSE(filename.empty());
    }
}

// DEV NOTE: These tests should not be here; they are written in the completely wrong place.
//           I doubt we will ever use this code, but if we do, these test should be moved
//           down to the implementation they're testing, i.e. opencl/eight_million_point/.

#ifdef ENABLE_OPENCL
TEST_F(CxftTest, test_cxft_emulator)
{
    ska::rabbit::cxft::Cxft cxft;
    // This tests a subarchitecture, so it should be tested at that level, not here.
    // This test is now wrapped in an #ifdef, so will not run unless OpenCL is
    // enabled and the eight_million_point subarchitecture can be used.
    std::string filename = cxft.eight_million_point_image("emulator");
    ASSERT_FALSE(filename.empty());
}

// This test relies entirely on OpenCL, so should be tested under that architecture.
// It is now wrapped in an #ifdef so that it will not be performed unless OpenCL is enabled.
TEST_F(CxftTest, test_cxft_eight_million_point_image_from_board_name)
{
    ska::rabbit::cxft::Cxft cxft;
    auto image_manager = cxft.eight_million_point_image_manager();
    for(auto const& board : image_manager.images())
    {
        std::string filename = image_manager.image_path(board);
        ASSERT_FALSE(filename.empty());
    }
}
#endif // ENABLE_OPENCL

} // namespace test
} // namespace cxft
} // namespace rabbit
} // namespace ska
