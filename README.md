Low-level, accelerator-specific algorithms and FPGA image repository for use with ska-pss-panda and ska-pss-cheetah

# The repository
This project contains various FPGA-compiled images that
relate to pulsar searching as well as a thin C++ wrapper
library for accessing these images. C++ clients can
request a suitable FPGA image via the ImageManager object
associated with each functional module.

The repository is split into functional modules (which mirror
the modules in ska-pss-cheetah) that may have more than one
implementation (e.g VHDL or OpenCL, different FPGA chips, etc).
The code included in these modules is specific to a particular
implementation of that module (e.g. fdas/intel_agilex_fpga/)
and is low-level enough (for example, initialising algorithm
parameters that are not user-configurable) that it should not
be publicly exposed in cheetah, but is algorithmic in nature,
so does not belong in panda.

# Example
```c++
#include "ska/rabbit/Ddtr.h"

std::string fpga_board("p510t_sch_ax115");

// Get a dedispersion image from rabbit
ska::rabbit::Ddtr ddtr;
auto image_filename = ddtr.image_manager().image_path(fpga_board);

```

# Installation
This is a CMake-based project.
Please see CMakeLists.txt file for more build instructions.

Common CMake flags:

-DRABBIT_MODULE: only build a particular rabbit module

-DENABLE_OPENCL: set true to compile OpenCL code. OpenCL requires
                 you to have the Altera tool chain installed, and
                 the following dependencies must be set:

    -DCMAKE_AOC_COMPILER: set aoc compiler path
    -DOpenCL_INSTALL_DIR: path to include OpenCL files and library
    -DALTERA_FPGA_EMULATION: set to true to compile emulator-only
    -DAOC_BSP: specify path to BSP package of installed device

# Source Code
The source code can be found under the rabbit/ directory

# Contributing
Please see doc/developers_guide.md
